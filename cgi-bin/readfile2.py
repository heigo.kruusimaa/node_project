#!/usr/bin/python
import cgi, cgitb
from datetime import datetime, time

print "Content-type: text/html"
print

cgitb.enable()

ords = [1, 1, 1, 1, 1, 1]
formdata = cgi.FieldStorage()
if formdata.has_key('col'):
    colnr = formdata['col'].value
    if formdata['order'].value == "1":
	ords[int(colnr)] = -1
    else:
	ords[int(colnr)] = 1
name = ""
if formdata.has_key('nimi'):
    name = "&nimi=" + formdata['nimi'].value
filtr = ""
addfilt = ""
if formdata.has_key('filter'):
    filtr = formdata['filter'].value
    addfilt = "filter=" + filtr + "&"
filt1 = ""
filt2 = ""
filt3 = ""
if filtr == 'multi':
    filt3 = 'checked'
elif filtr == 'single':
    filt2 = 'checked'
else:
    filt1 = 'checked'
header = "<html><head><meta charset='utf-8'>"
meta = "<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no' />"
css1 = "<link rel='stylesheet' href='../prax3/prax2.css' media='only screen and (min-width: 640px)'>"
css2 = "<link rel='stylesheet' href='../prax3/prax2.css' media='handheld, only screen and (max-width: 640px), only screen and (max-device-width: 480px)'>"
script1 = "<script src='../jquery-2.1.1.min.js'></script>"
script2 = "<script src='../prax3/filtertable.js'></script>"
endheader = "<title>Score table</title></head>"
body = "<body><div id='header'><h2>Game history</h2><a href='http://127.0.0.1:1337'>Back to game</a></div><div class='wrapper'>"
form = "<form name='input' method='get' action='readfile2.py'>Sisesta m&auml;ngija nimi: <input type='text' name='nimi'><input type='submit' value='Submit'></form>"
radio = "<div class='rdios'><input type='radio' name='table' id='all' " + filt1 + " value='all'><label for='all'>All games</label></div><div class='rdios'><input type='radio' name='table' id='single' " + filt2 + " value='single'><label for='single'>Single player</label></div><div class='rdios'><input type='radio' name='table' id='multi' " + filt3 + " value='multi'><label for='multi'>Multiplayer</label></div>"
table = "<table><thead><tr>"
thead1 = "<th scope='col'><a href='readfile2.py?" + addfilt + "col=0&order=" + str(ords[0]) + name + "'>Start time</a></th>"
thead2 = "<th scope='col'><a href='readfile2.py?" + addfilt + "col=1&order=" + str(ords[1]) + name + "'>Player 1 name</a></th>"
thead3 = "<th scope='col'><a href='readfile2.py?" + addfilt + "col=2&order=" + str(ords[2]) + name + "'>Player 1 score</a></th>"
thead4 = "<th scope='col'><a href='readfile2.py?" + addfilt + "col=3&order=" + str(ords[3]) + name + "'>Player 2 name</a></th>"
thead5 = "<th scope='col'><a href='readfile2.py?" + addfilt + "col=4&order=" + str(ords[4]) + name + "'>Player 2 score</a></th>"
thead6 = "<th scope='col'><a href='readfile2.py?" + addfilt + "col=5&order=" + str(ords[5]) + name + "'>Game duration</a></th>"
endhead = "</tr></thead><tbody>"

f = open("../www-data/data.csv", "r")
line = f.readline();
rows = []
while line:
    i = 0
    values = []
    values = line.split(",")
    if filtr == "multi":
        if len(values) > 6 and len(values) < 9:
            rows.append([values[i+4] + ", " + values[i+5],values[0],int(values[i+1]),values[i+2],int(values[i+3]),int(values[i+6])])
    elif filtr == "single":
        if len(values) > 1 and len(values) < 6 or len(values) > 7:
	    while len(values) > (i+1):
                rows.append([values[i+2] + ", " + values[i+3],values[0],int(values[i+1]),"-","-",int(values[i+4])])
		i = i + 4
    else:
        if len(values) > 6 and len(values) < 9:
            rows.append([values[i+4] + ", " + values[i+5],values[0],int(values[i+1]),values[i+2],int(values[i+3]),int(values[i+6])])
        elif len(values) > 1 and len(values) < 6 or len(values) > 7:
	    while len(values) > (i+1):
                rows.append([values[i+2] + ", " + values[i+3],values[0],int(values[i+1]),"-","-",int(values[i+4])])
		i = i + 4
    line = f.readline();
endtable = "</tbody></table></div></body>"

def tabledata():
    result = ""
    if formdata.has_key('nimi'):
	for fileline in rows:
	    if fileline[1] == formdata['nimi'].value:
        	result += "<tr><td>" + "</td><td>".join(str(x) for x in fileline) + "</td></tr>"
    else:
    	for fileline in rows:
            result += "<tr><td>" + "</td><td>".join(str(x) for x in fileline) + "</td></tr>"
    return result

if formdata.has_key('col'):
    col = formdata['col'].value
    if formdata['order'].value == "1":
        asc = False
    else:
        asc = True
    if col == "0":
	rows = sorted(rows, key=lambda l:datetime.strptime(l[int(col)], "%a, %d %b %Y %H:%M:%S %Z"), reverse = asc)
    else:
        rows = sorted(rows, key=lambda l:l[int(col)], reverse=asc)

print header+meta+css1+css2+script1+script2+endheader+body+form+radio+table+thead1+thead2+thead3+thead4+thead5+thead6+endhead+tabledata()+endtable

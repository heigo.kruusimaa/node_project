#!/usr/bin/python
import cgi, cgitb, json, os
from datetime import datetime
cgitb.enable()

formdata = cgi.FieldStorage()
filename = "../prax3/queue"
pattern = "%H:%M:%S"
test = ""

def readData():
    f = open(filename, "r")
    data = f.read()
    f.close()
    jsonLines = data.split("\n")
    return jsonLines

def redirect(pl1, pl2, gamefile):
    f = open("../prax3/template.html", "r")
    template = f.read()
    f.close()
    template = template.replace("{Player1}", "\"" + pl1 + "\"")
    template = template.replace("{Filename}", "\"" + gamefile + "\"")
    print "Content-type: text/html"
    print
    print template

if formdata.has_key('name'):
    f = open(filename, "a")
    jsonData = {}
    jsonData[formdata['name'].value] = datetime.now().strftime(pattern)
    f.write(json.dumps(jsonData) + "\n")
    f.close()
    print
if formdata.has_key('poll'):
    result = []
    name = formdata['poll'].value
    jsonLines = readData()
    currentime = datetime.now().strftime(pattern)
    for i in range(len(jsonLines)-1):
        temp = json.loads(jsonLines[i])
        if temp.keys()[0] == name:
            if len(temp[name].split(':')) == 1:
                test = temp[name]
                continue
            else:
                if (datetime.strptime(currentime, pattern) - datetime.strptime(temp[temp.keys()[0]], pattern)).seconds > 60:
                    continue;
                else:
                    temp[name] = datetime.now().strftime(pattern)
                    result.append(temp)
        elif (datetime.strptime(currentime, pattern) - datetime.strptime(temp[temp.keys()[0]], pattern)).seconds > 60:
            continue
        else:
            result.append(temp)
    f = open(filename, "w")
    for line in result:
        f.write(json.dumps(line) + "\n")
    f.close()
    if len(test) > 0:
        print "Content-type: text/plain"
        print
        print "Opponent " + test
    print
if formdata.has_key('pl1'):
    jsonLines = readData()
    result = []
    pl1 = formdata['pl1'].value
    pl2 = formdata['pl2'].value
    for i in range(len(jsonLines)-1):
        temp = json.loads(jsonLines[i])
        if temp.keys()[0] == pl2:
            temp[pl2] = pl1
        result.append(temp)
    f = open(filename, "w")
    for line in result:
        f.write(json.dumps(line) + "\n")
    f.close()
    gamefile = "../www-data/" + pl1 + "_" + pl2
    f = open(gamefile, "w")
    f.write(json.dumps({pl1: "start"}))
    f.close()
    redirect(pl1, pl2, pl1 + "_" + pl2)
if formdata.has_key('p1'):
    p1 = formdata['p1'].value
    p2 = formdata['p2'].value
    redirect(p1, p2, p2 + "_" + p1)

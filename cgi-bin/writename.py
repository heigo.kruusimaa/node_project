#!/usr/bin/python
import cgi, cgitb, os.path;

cgitb.enable()

form = cgi.FieldStorage()
fn = ""
try:
    fn = form['Name'].value
except KeyError:
    print "Location: http://127.0.1.1/prax3"
    print

filepath = '../www-data/data.csv'
if os.path.isfile(filepath):
    f = open(filepath, 'a')
    f.write("\n")
    f.write(fn)
else:
    f = open(filepath, 'w')
    f.write(fn)

f.close()

print "Location: http://127.0.1.1/prax3/game.html"
print

#!/usr/bin/python
import cgi, cgitb, json
cgitb.enable()

formdata = cgi.FieldStorage()
filename = "../www-data/" + formdata['filename'].value
if not formdata.has_key('name'):
    f = open(filename, 'r')
    jsonLine = f.read()
    f.close()
    print "Content-type: text/plain"
    print
    print jsonLine
else:
    f = open(filename, "w")
    f.write(json.dumps({formdata['name'].value: int(formdata['score'].value)}))
    f.close()
    print

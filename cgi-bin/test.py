#!/usr/bin/python
import cgi, cgitb, os

cgitb.enable()

f = open("../www-data/data.csv", "a")
formdata = cgi.FieldStorage()
if formdata.has_key('pl1'):
    line = "\n" + formdata['pl1'].value + "," + formdata['score1'].value + "," + formdata['pl2'].value + "," + formdata['score2'].value + "," + formdata['startTime'].value + "," + formdata['duration'].value
    f.write(line)
    os.remove("../www-data/" + formdata['pl1'].value + "_" + formdata['pl2'].value)
else:
    f.write(',' + formdata['score'].value)
    f.write(',' + formdata['startTime'].value)
    f.write(',' + formdata['duration'].value)

f.close()
print

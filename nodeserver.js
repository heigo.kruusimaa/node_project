var http = require('http');
var fs = require('fs');
var path = require('path');
var qs = require('querystring');

http.createServer(function (request, response) {
    console.log('request starting...');
	
	var filePath = '.' + request.url;
	if (filePath == './')
		filePath = './index.html';
		
	var extname = path.extname(filePath);
	var contentType = 'text/html';

	switch (extname) {
		case '.js':
			contentType = 'text/javascript';
			break;
		case '.css':
			contentType = 'text/css';
			break;
	}
	
	fs.exists(filePath, function(exists) {
	
		if (exists) {
			fs.readFile(filePath, function(error, content) {
				if (error) {
					response.writeHead(500);
					response.end();
				}
				else {
					response.writeHead(200, { 'Content-Type': contentType });
					response.end(content, 'utf-8');
				}
			});
		}
		else {
			response.writeHead(404);
			response.end();
		}
	});

    if (request.method === "POST") {
        if (request.url === "/game.html") {
            var requestBody = '';
            request.on('data', function(data) {
                requestBody += data;
            });
            request.on('end', function() {
                var formData = qs.parse(requestBody);
                var score1 = 0;
                var score2 = 0;
                var trn = 1;
                var files = fs.readdir('./tmp/', function(err, data) {
                    if (err) {
                        console.log(err);
                    } else {
                        if (data.indexOf(formData.name) == -1) {
                            fs.writeFile("./tmp/" + formData.name , JSON.stringify({"turn": 1, "score1": 0, "score2": 0}), function(err) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    console.log("JSON file saved to " + formData.name);
                                }
                            });
                        } else {
                            /*
                            fs.readFile("./tmp/" + formData.name, function(err, content) {
                                var obj = JSON.parse(content);
                                score1 = obj.score1;
                                score2 = obj.score2;
                            });
                            */
                            var contents = fs.readFileSync("./tmp/" + formData.name);
                            var obj = JSON.parse(contents);
                            if (obj.turn == 1) trn = 2;
                            score1 = obj.score1;
                            score2 = obj.score2;
                        }
                    }
                });

                fs.readFile('./game.html', 'utf-8', function(error, content) {
                    if (error) {
                        response.writeHead(500);
                        response.end(error);
                    } else {
                        content = content.replace("{{name}}", "\"" + formData.name + "\"");
                        content = content.replace("{{score1}}", score1);
                        content = content.replace("{{score2}}", score2);
                        content = content.replace("{{turn}}", trn);
                        response.writeHead(200, { 'Content-type': 'text/html' });
                        response.end(content, 'utf-8');
                    }
                });
            });
        } else if (request.url === '/gamestate') {
            var gameInfo = '';

            request.on('data', function(data) {
                gameInfo += data;
            });

            request.on('end', function() {
                var form = qs.parse(gameInfo);

                if (form.score1 >= 100 || form.score2 >= 100) {
                    fs.unlink('./tmp/' + form.name, function(err) {
                        if (err) console.log(err);
                    });
                } else {
                    fs.writeFile('./tmp/' + form.name , JSON.stringify({"turn": form.turn, "score1": form.score1, "score2": form.score2}),function(err){
                        if (err) {
                            console.log(err);
                        } else {
                            console.log("Game updated");
                        }
                    });
                }
            });

            response.end();
        } else if (request.url === '/reset') {
            fs.writeFile("./tmp/" + formData.name , JSON.stringify({"turn": 1, "score1": 0, "score2": 0}), function(err) {
               if (err) {
                    console.log(err);
                } else {
                    console.log("JSON file saved to " + formData.name);
                }
            });
        }
    } else if (request.method === "GET") {
        if (request.url === '/getdiceval') {
            var first = Math.floor((Math.random() * 6) + 1);
            var second = Math.floor((Math.random() * 6) + 1);
            response.writeHead(200, {'Content-type': 'text/plain'});
            response.end(first + "," + second);
        }
    }
	
}).listen(1337);

console.log('Server running at http://127.0.0.1:1337/');

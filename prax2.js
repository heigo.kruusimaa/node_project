var currentPics = [0 ,0];
var sum = 0, limit = 20;
var time;
var select = document.querySelector("select");
var mq = window.matchMedia("only screen and (min-width: 640px)");
window.onload = function() {
    setText("userscore", userTotal);
    setText("compscore", compTotal);
    if (turn == 2) computerMove();
};
function rollDice() {
    for(var i = 0; i < 2; i++) {
        var num = Math.floor((Math.random() * 6) + 1);
        changePics(i, num);    
    }
}
function changePics(i, num) {
    var pics = document.getElementsByTagName("img");
    switch(num) {
        case 1: pics[i].src = "One_die.png"; break;
        case 2: pics[i].src = "Two_die.png"; break;
        case 3: pics[i].src = "Three_die.png"; break;
        case 4: pics[i].src = "Four_die.png"; break;
        case 5: pics[i].src = "Five_die.png"; break;
        case 6: pics[i].src = "Six_die.png"; break;
    }
}
function checkState() {
    if(currentPics[0] == currentPics[1]) {
        if(currentPics[0] == 1) {
            sum += 25;
        } else {
            sum += 4 * currentPics[0];
        }
        setText("currentscore", sum);
        return;
    } else if(currentPics[0] == 1 || currentPics[1] == 1) {
        sum = 0;
        setText("currentscore", sum);
        return 0;
    } else {
        sum += currentPics[0] + currentPics[1];
        setText("currentscore", sum);
        return;
    }
}
function doRoll() {
    getDiceVal();
    if(time === null) time = new Date();
    var i = setInterval(rollDice, 125);
    setTimeout(function() {
            clearInterval(i);
            changePics(0,currentPics[0]);
            changePics(1,currentPics[1]);
            var result = checkState();
            if(result === 0) {
                setText("userturn", '  + ' + 0);
                fade("userturn");
		if(mq.matches) {
                    createParagraph(1, 0);
		}
                turn = 1;
                sendData();
                setTimeout(computerMove, 500);
            }
            if(userTotal + sum >= 100) {
                userTotal += sum;
                setText("userturn", '+' + sum);
                fade("userturn");
		if(mq.matches) {
                    createParagraph(1, sum);
		}
                setText("userscore", userTotal);
                setText("result", "You win!");
                gid("roll").disabled = true;
                gid("stop").disabled = true;
                turn = 1;
                sendData();
                gid("again").style.visibility = "visible";
                return;
            }
            }, 500);
}
function saveScore() {
    if(sum === 0) return;
    else {
        userTotal += sum;
        setText("userscore", userTotal);
        setText("userturn",'+' +  sum);
        fade("userturn");
	if(mq.matches) {
            createParagraph(1, sum);
	}
        sum = 0;
        setText("currentscore", sum);
    }
    turn = 1;
    sendData();
    computerMove();
}
function computerMove() {
    setText("result", "Opponents turn");
    gid("roll").disabled = true;
    gid("stop").disabled = true;
    getDiceVal();
    var i = setInterval(rollDice, 125);
    setTimeout(function() {
            clearInterval(i);
            changePics(0,currentPics[0]);
            changePics(1,currentPics[1]);
            var result = checkState();
            setTimeout(function() {
            if(result === 0) {
                setText("compturn", '+ ' +  0);
                fade("compturn");
		if(mq.matches) {
                    createParagraph(0, 0);
		}
                setText("result", "Your turn");
                gid("roll").disabled = false;
                gid("stop").disabled = false;
                turn = 2;
                sendData();
                return;
            }
            if(compTotal + sum >= 100) {
                compTotal += sum;
                setText("compturn", '+' + sum);
                fade("compturn");
		if(mq.matches) {
                    createParagraph(0, sum);
		}
                setText("compscore", compTotal);
                setText("result", "You lose!");
                gid("roll").disabled = true;
                gid("stop").disabled = true;
                turn = 2;
                sendData();
                gid("again").style.visibility = "visible";
                return;
            }
            if(sum < limit) {
                setTimeout(computerMove, 500);
            } else {
                compTotal += sum;
                setText("compturn", '+' + sum);
		if(mq.matches) {
                    createParagraph(0, sum);
		}
                sum = 0;
                turn = 2;
                sendData();
                gid("roll").disabled = false;
                gid("stop").disabled = false;
                setText("result", "Your turn");
                fade("compturn");
            }
            setText("currentscore", sum);
            setText("compscore", compTotal);
            }, 500);
            }, 500);
}
function reset() {
    sum = 0;
    compTotal = 0;
    userTotal = 0;
    time = null;
    var pics = document.getElementsByTagName("img");
    pics[0].src = "One_die.png";
    pics[1].src = "One_die.png";
    setText("currentscore", sum);
    setText("userscore", userTotal);
    setText("compscore", compTotal);
    setText("result", "Your turn");
    gid("roll").disabled = false;
    gid("stop").disabled = false;
    gid("again").style.visibility = "hidden";
    removeChilds("userscoretable");
    removeChilds("compscoretable");
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open('POST', '/reset', true);
    xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xmlhttp.send('name=' + name);
}
function changeLimit(sel) {
    limit = sel.value;
}
function gid(x) {
    return document.getElementById(x);
}
function setText(el, val) {
    gid(el).innerHTML = val;
}
function fade(el) {
    var element = gid(el);
    element.style.animation = "fadein 1s";
    element.style.opacity = "1";
    setTimeout(function() {
            element.style.animation = "fadeout 1s";
            element.style.opacity = "0";
            }, 500);
}
function createParagraph(player, score) {
    var par = document.createElement("p");
    var text = document.createTextNode("+" + score);
    par.appendChild(text);
    if(player == 1) {
        gid("userscoretable").appendChild(par);
    } else {
        gid("compscoretable").appendChild(par);
    }
}
function removeChilds(nodeName) {
    var last;
    var node = gid(nodeName);
    while(last = node.lastChild) node.removeChild(last);
}
function sendData() {
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open('POST', '/gamestate', true);
    xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xmlhttp.send('name=' + name + '&turn=' + turn + '&score1=' + userTotal + '&score2=' + compTotal);
}
function showTable() {
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open('GET', 'http://127.0.1.1/cgi-bin/readfile2.py', true);
    xmlhttp.setRequestHeader('Content-type','text/html');
    xmlhttp.send();
}
function getDiceVal() {
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open('GET', '/getdiceval', false);
    //xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xmlhttp.send();
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        currentPics = xmlhttp.responseText.split(",").map(Number);
        console.log(currentPics[0] + " " + currentPics[1]);
    }
}
